# clang-git
Alpine-based Clang and Git Docker image. Primarily aimed on using clang-format in GitLab CI/CD pipelines.

Images are tagged with the Clang version string. Images tagged with just the major Clang version number and "latest" image are also provided.

To build the image, choose an [available `clang-extra-tools` version](https://pkgs.alpinelinux.org/packages?name=clang-extra-tools) for the latest stable Alpine branch and build. Example:
```shell
docker build . --build-arg CLANG_VERSION==9.0.0-r1 -t maranov/clang-git:9.0.0-r1
```

`ALPINE_VERSION` can be alternatively used to build for edge or legacy Alpine branches.

The code style can be checked and a diff retrieved by e.g. applying the changes in-place and querying git for changes:
```sh
#!/usr/bin/env sh

for f in $(find './src' './include' | grep -E '.+\.(c|cc|cpp|cxx|c\+\+|h|hh|hpp|hxx|h\+\+)(\.in)?$'); do
    clang-format -i --style=file "$f"
done

git diff > clang-format.patch
if [ -s clang-format.patch ]; then
    exit 1
else
    exit 0
fi
```
