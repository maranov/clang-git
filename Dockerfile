# Base image version.
ARG ALPINE_VERSION=latest
FROM alpine:${ALPINE_VERSION}
# clang apk version specifier.
ARG CLANG_VERSION=
RUN apk add --no-cache clang${CLANG_VERSION} clang-extra-tools${CLANG_VERSION} git
